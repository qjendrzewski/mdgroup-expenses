<?php

namespace Tests\Feature\Http;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Expense as ExpenseModel;

/**
 * Tests HTTP requests to get all expenses
 * 
 * @category tests
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */

class GetAllTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Tests that no expenses are retrieved
     *
     * @return void
     */
    public function testGetAllHasNoData()
    {

        $oResponse = $this->get('/api/v1/expense/all');        
        $oResponse->assertStatus(200);

        $arrResponse = $oResponse->json();

        $this->assertEquals( 0, $arrResponse['result'] );
        $this->assertArrayHasKey('reason', $arrResponse);
        $this->assertEquals( 'No expenses data exists', $arrResponse['reason'] );

    }

    /**
     * Tests that all stored expenses are retrieved
     *
     * @return void
     */
    public function testGetAllHasData()
    {

        $oExpense = ExpenseModel::factory()->count(10)->create();

        $oResponse = $this->get('/api/v1/expense/all');        
        $oResponse->assertStatus(200);

        $arrResponse = $oResponse->json();
        $arrData = $arrResponse['data'];

        $this->assertEquals( 1, $arrResponse['result'] );
        $this->assertArrayHasKey('data', $arrResponse);
        $this->assertCount( 10, $arrData );

    }

}