<?php
namespace Tests\Feature\Http;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

/**
 * Tests HTTP requests to add expenses
 * 
 * @category tests
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class PostTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Tests call to add expense when no data provided
     * 
     *
     * @return void
     */
    public function testMissingParamsAllRequestFails()
    {
        $response = $this->post('/api/v1/expense');

        $response->assertJson(
            [
                "result" =>  0,
                "reason" => "Validation failure",
                "amount" => [
                        "The amount is missing"
                ],
                "desc" => [
                    "The description is missing"
                ],
                "type" => [
                    "The expense type is missing"
                ]
            ]
        );
        
        /**
         * Asserts whether HTTP status is 422
         */
        $response->assertUnprocessable();
    }

    /**
     * Tests call to add expense when no amount provided
     * 
     *
     * @return void
     */
    public function testMissingParamsAmountRequestFails()
    {
        $response = $this->post(
            '/api/v1/expense', 
            [
                'desc' => 'A description text',
                'type' => 'food',
            ]
        );

        $response->assertJson(
            [
                "result" =>  0,
                "reason" => "Validation failure",
                "amount" => [
                        "The amount is missing"
                ],
            ]
        );
        
        /**
         * Asserts whether HTTP status is 422
         */
        $response->assertUnprocessable();
    }

    /**
     * Tests call to add expense when no description provided
     * 
     *
     * @return void
     */
    public function testMissingParamsDescRequestFails()
    {
        $response = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'type' => 'food',
            ]
        );

        $response->assertJson(
            [
                "result" =>  0,
                "reason" => "Validation failure",
                "desc" => [
                    "The description is missing"
                ],
            ]
        );
        
        /**
         * Asserts whether HTTP status is 422
         */
        $response->assertUnprocessable();
    }

    /**
     * Tests call to add expense when no type provided
     * 
     *
     * @return void
     */
    public function testMissingParamsTypeRequestFails()
    {
        $response = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text',
            ]
        );

        $response->assertJson(
            [
                "result" =>  0,
                "reason" => "Validation failure",
                "type" => [
                    "The expense type is missing"
                ]
            ]
        );
        
        /**
         * Asserts whether HTTP status is 422
         */
        $response->assertUnprocessable();
    }

    /**
     * Tests call to add expense when bad type provided
     * 
     *
     * @return void
     */
    public function testBadParamsTypeRequestFails()
    {
        $response = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text',
                'type' => 'frank',
            ]
        );

        $response->assertJson(
            [
                "result" =>  0,
                "reason" => "Validation failure",
                "type" => [
                    "The supplied expense type must be one of 'entertainment', 'food', 'bills', 'transport', or 'other'",
                ]
            ]
        );
        
        /**
         * Asserts whether HTTP status is 422
         */
        $response->assertUnprocessable();
    }

    /**
     * Tests call to add expense when bad type provided
     * 
     *
     * @return void
     */
    public function testGoodRequestSucceeds()
    {

        $this->seed();

        $oResponse = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text testGoodRequestSucceeds',
                'type' => 'entertainment',
            ]
        );
        
        /**
         * Asserts whether HTTP status is 201
         */
        $oResponse->assertCreated();

    }

    /**
     * Tests call to add expense when bad type provided
     * 
     *
     * @return void
     */
    public function testGoodRequestSucceedsResult()
    {

        $oResponse = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text testGoodRequestSucceedsResult',
                'type' => 'entertainment',
            ]
        );

        $arrResponse = $oResponse->json();
        $arrData = $arrResponse['data'];
        $this->assertEquals( 1, $arrResponse['result'] );
        
    }

    /**
     * Tests call to add expense when bad type provided
     * 
     *
     * @return void
     */
    public function testGoodRequestSucceedsResultsDataExists()
    {

        $oResponse = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text testGoodRequestSucceedsResultsDataExists',
                'type' => 'entertainment',
            ]
        );
        
        /**
         * Asserts whether HTTP status is 201
         */
        $oResponse->assertCreated();        

        $arrResponse = $oResponse->json();
        $arrData = $arrResponse['data'];
        $this->assertArrayHasKey('data', $arrResponse);
        
    }

    /**
     * Tests call to add expense when bad type provided
     * 
     *
     * @return void
     */
    public function testGoodRequestSucceedsResultsDataEquals()
    {

        $oResponse = $this->post(
            '/api/v1/expense', 
            [
                'amount' => 1.23,
                'desc' => 'A description text testGoodRequestSucceedsResultsDataEquals',
                'type' => 'entertainment',
            ]
        );
        
        /**
         * Asserts whether HTTP status is 201
         */
        $oResponse->assertCreated();        

        $arrResponse = $oResponse->json();
        $arrData = $arrResponse['data'];
        
        $this->assertArrayHasKey('id', $arrData);

        unset( $arrData['id'] );

        $this->assertEquals( 
            [
                'amount' => 1.23,
                'desc' => 'A description text testGoodRequestSucceedsResultsDataEquals',
                'type' => 'entertainment',
            ],
            $arrData
        );
        
    }
    

}
