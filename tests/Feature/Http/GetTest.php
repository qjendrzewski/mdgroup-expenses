<?php
namespace Tests\Feature\Http;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Database\Seeders\GetTestSeeder;


/**
 * Tests HTTP requests to get an expense
 * 
 * @category tests
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class GetTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Tests get expense by Id
     *
     * @return void
     */
    public function testGetById()
    {
        $this->seed(GetTestSeeder::class);

        $oResponse = $this->get('/api/v1/expense?id=2');

        $oResponse->assertOk();
        $arrResponse = $oResponse->json();

        $this->assertArrayHasKey('result', $arrResponse);
        $this->assertEquals( 1, $arrResponse['result'] );
        
        $this->assertArrayHasKey('data', $arrResponse);
        $arrData = $arrResponse['data'];
        $this->assertEquals( 2, $arrData['id'] );

    }

    /**
     * Tests get expense by Id
     *
     * @return void
     */
    public function testGetByDescriptionAndType()
    {

        $oResponse = $this->get('/api/v1/expense?desc=am&type=food');

var_dump($oResponse);

        $oResponse->assertOk();
        $arrResponse = $oResponse->json();

        $this->assertArrayHasKey('result', $arrResponse);
        $this->assertEquals( 1, $arrResponse['result'] );
        
        $this->assertArrayHasKey('data', $arrResponse);
        $arrData = $arrResponse['data'];
        $this->assertEquals( 'I am two', $arrData['description'] );
        $this->assertEquals( 2.22, $arrData['amount'] );
        $this->assertEquals( 'Food', $arrData['expense_type'] );
        

    }

}