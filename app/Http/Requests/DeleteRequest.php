<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstracts\ApiFormRequest;

/**
 * Validation request for getting a single expense
 * 
 * @url /api/v1/expense
 * @method DELETE
 * 
 * @category request
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class DeleteRequest extends ApiFormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:expenses,id',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'The Expense Id is missing',
            'id.integer' => 'The Expense Id must be a whole number greater than zero',
            'id.exists' => 'The supplied expense id doesn\'t exist',
        ];
    }

}