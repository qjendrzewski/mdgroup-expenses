<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstracts\ApiFormRequest;

/**
 * Validation request for updating a single expense
 * 
 * @url /api/v1/expense
 * @method PUT
 * 
 * @category request
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class PutRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|exists:expenses,id',
            'desc' => 'sometimes',
            'amount' => 'sometimes|numeric',
            'type' => 'sometimes|in:entertainment,food,bills,transport,other',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'The Expense Id is missing',
            'id.integer' => 'The Expense Id must be a whole number larger than zero',
            'id.exists' => 'The suppled Expense Id does\'t exist',
            'amount.numeric' => 'The amount must be a number',
            'type.in' => "The supplied expense type must be one of 'entertainment', 'food', 'bills', 'transport', or 'other'",
        ];
    }

}