<?php
namespace App\Http\Requests;

use App\Http\Requests\Abstracts\ApiFormRequest;

/**
 * Validation request for adding expense
 * 
 * @url /api/v1/expense
 * 
 * @category request
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class PostRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'desc' => 'required',
            'type' => 'required|in:entertainment,food,bills,transport,other',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.required' => 'The amount is missing',
            'amount.numeric' => 'The amount must be a number',
            'desc.required' => 'The description is missing',
            'type.required' => 'The expense type is missing',
            'type.in' => "The supplied expense type must be one of 'entertainment', 'food', 'bills', 'transport', or 'other'",
        ];
    }

}