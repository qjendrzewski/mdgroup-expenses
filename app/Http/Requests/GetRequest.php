<?php
namespace App\Http\Requests;

use App\Http\Requests\Abstracts\ApiFormRequest;

/**
 * Validation request for getting a single expense
 * 
 * @url /api/v1/expense
 * 
 * @category request
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class GetRequest extends ApiFormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'sometimes|required_if:desc,null|integer|exists:expenses,id',
            'desc' => 'sometimes|required_if:id,null',
            'type' => 'sometimes|required_if:id,null|in:entertainment,food,bills,transport,other',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required_if' => 'Provide either an expense id, or description and expense-type',
            'desc.required_if' => 'Provide either an expense id, or description and expense-type',
            'type.required_if' => 'Provide either an expense id, or description and expense-type',
            'id.integer' => 'The expense id must be a whole positive number',
            'id.exists' => 'The supplied expense id doesn\'t exist',
            'type.in' => "The supplied expense type must be one of 'entertainment', 'food', 'bills', 'transport', or 'other'",
        ];
    }

}