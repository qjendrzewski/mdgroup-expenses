<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

/**
 * Logs API requests and responses
 * 
 * @see https://lessthan12ms.com/how-to-log-every-http-request-and-response-in-laravel.html#log-http-requests-and-responses-in-laravel
 * 
 * @category middleware
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class ApiLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Terminable action after response sent
     * 
     * @see https://laravel.com/docs/7.x/middleware#terminable-middleware
     * @see https://gist.github.com/Shelob9/174f6093c6b388f325157ba3dea82a46
     * 
     * @param Illuminate\Http\Request  $p_oRequest  Request instance
     * @param Illuminate\Http\Response $p_oResponse Response instance
     * 
     * @return void
     */
    public function terminate($p_oRequest, $p_oResponse)
    {        
        if ( 1 == env('INTERNAL_API_LOG') ) {

            $arrResponse = json_decode( $p_oResponse->getContent(), 1 );
            if (!$arrResponse) {
                $arrResponse = [];
            }

            $message = implode(
                "\n",
                [
                    "URL = " . $p_oRequest->fullUrl(),
                    $this->_logRequest(),                
                    $this->_logResponse($arrResponse),
                    "HTTP STATUS = " . $p_oResponse->status(),
                    "FINISH\n\n\n\n\n",
                ]
            );
            Log::channel('apilog')->info($message);

        }

    }
    
    
    /**
     * Writes the request to the Log
     * 
     * @return string Log message
     */
    private function _logRequest()
    {
        /**
         * Defining array of data-fields to mask in logs
         */
        $arrMask = [
            'appid',
        ];
        
        $arrData = request()->toArray();
        foreach ( $arrMask as $key=>$value ) {
            if ( array_key_exists($value, $arrData) ) {
                $arrData[$value] = "*****";
            }
        }
        
        $arrLog = [
            'data' => $arrData,
            'ip' => request()->ip(),
            'user_agent' => request()->userAgent(),
        ];

        return "REQUEST = " . print_r( (array)$arrLog, 1 );
    }
    
    
    /**
     * Writes the response to the Log
     * 
     * @param array $p_arrResponse Response
     * 
     * @return string Log message
     */
    protected function _logResponse(array $p_arrResponse=[])
    {
        
        /**
         * Checking whether there is a 'data' element in the response payload
         */
        if ( array_key_exists( 'data', $p_arrResponse ) ) {
            
            /**
             * Defining array of data-fields to mask in logs
             */
            $arrMask = [
                'appid',
            ];

            foreach ( $arrMask as $key=>$value ) {
                if ( array_key_exists( $value, $p_arrResponse['data'] ) ) {
                    $p_arrResponse['data'][$value] = "*****";
                }
            }
            
        }

        return "RESPONSE = " . print_r($p_arrResponse, 1);
    }

}