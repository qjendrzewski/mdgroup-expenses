<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Http\Requests\DeleteRequest;

/**
 * The controller to serve API requests to delete an expense
 * 
 * @url /expense
 * @method DELETE
 * 
 * @param integer $_GET['id] Expense Id
 * 
 * @category controllers
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class DeleteController extends Controller
{

    public function __invoke(DeleteRequest $p_oRequest)
    {

        $arrData = $p_oRequest->validated();

        try {

            $success = \App\Models\Expense::find( $arrData['id'] )->delete();

        } catch (\Exception $e) {

            return response()->json( 
                [
                    'result' => 0,
                    'reason' => $e->getMessage(),
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );

        }

        if ($success) {

            return response()->json( 
                [
                    'result' => 1,
                ],
                JsonResponse::HTTP_OK
            );

        }

        return response()->json( 
            [
                'result' => 0,
                'reason' => 'The attempt to delete the expense failed',
            ],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        );
        
    }
    
}