<?php
namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * The controller to serve API requests to add an expense
 * 
 * @url /expense
 * @method POST
 * 
 * @category controllers
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class PostController extends Controller
{

    /**
     * Main method to add the data
     * 
     * @return void 
     */
    public function __invoke(PostRequest $p_oRequest)
    {

        $arrData = $p_oRequest->validated();

        try {

            $oType = \App\Models\ExpenseType::where( 'name', $arrData['type'] )->firstOrFail();

        } catch (ModelNotFoundException $e) {

            /**
             * Sending failure response
             */
            return response()->json(
                [
                    'result' => 0,
                    'reason' => 'Supplied type not found'
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );

        }

        try {
            
            
            $oExpense = \App\Models\Expense::create(
                [
                    'description' => $arrData['desc'],
                    'amount' => $arrData['amount'],
                    'expense_type' => $oType->id,
                ]
            );
                
        } catch (\Exception $e) {

            /**
             * Sending failure response
             */
            return response()->json(
                [
                    'result' => 0,
                    'reason' => $e->getMessage()
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );

        }

        /**
         * Sending success response
         */
        return response()->json(
            [
                'result' => 1,
                'data' => array_merge(
                    $arrData,
                    [
                        'id' => $oExpense->id,
                    ]
                )
            ],
            JsonResponse::HTTP_CREATED
        );

    }
    
}