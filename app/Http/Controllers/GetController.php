<?php
namespace App\Http\Controllers;

use App\Http\Requests\GetRequest;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

/**
 * The controller to serve API requests to add an expense
 * If the Expense Id is not supplied, then a fragment of an Expense Description and an Expense Type must be supplied
 * 
 * @param $_GET['id']          Expense Id
 * @param $_GET['description'] Description fragment
 * @param $_GET['type']        Expense type - one of 'entertainment', 'food', 'bills', 'transport', 'other'
 * 
 * @url /expense
 * @method GET
 * 
 * @category controllers
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class GetController extends Controller
{

    /**
     * @param GetRequest $p_oRequest Get Expense validation request object
     * 
     * @return JsonResponse 
     * 
     * @throws ValidationException 
     * @throws BindingResolutionException 
     */
    public function __invoke(GetRequest $p_oRequest)
    {
        
        $arrData = $p_oRequest->validated();

        if ( !empty( $arrData['id'] ) ) {

            $arrExpense = \App\Models\Expense::with('type')->find( $arrData['id'] )->toArray();

        } else {

            $oType = \App\Models\ExpenseType::where( 'name', $arrData['type'] )->first();
            if ($oType) {
                $oExpense = \App\Models\Expense::where( 'description', 'like', "%{$arrData['desc']}%" )
                                ->where('expense_type', $oType->id)                            
                                ->with('type')
                                ->first();
            } else {
                /**
                 * Sending failure response
                 */
                return response()->json(
                    [
                        'result' => 0,
                        'reason' => 'No matching expense-type found'
                    ],
                    JsonResponse::HTTP_UNPROCESSABLE_ENTITY
                );
            }

            if ( empty($oExpense) ) {

                /**
                 * Sending failure response
                 */
                return response()->json(
                    [
                        'result' => 0,
                        'reason' => 'No matching expense found'
                    ],
                    JsonResponse::HTTP_UNPROCESSABLE_ENTITY
                );

            }

            $arrExpense = $oExpense->toArray();

        }
        
        $arrExpense['expense_type'] = $arrExpense['type']['name'];
        unset( $arrExpense['type'] );

        /**
         * Sending success response
         */
        return response()->json(
            [
                'result' => 1,
                'data' => $arrExpense
            ],
            JsonResponse::HTTP_OK
        );
        
    }
    
}