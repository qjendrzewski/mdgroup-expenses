<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use  App\Http\Requests\PutRequest;

/**
 * The controller to serve API requests to update an expense
 * 
 * @url /expense
 * @method PUT
 * 
 * @category controllers
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class PutController extends Controller
{
    
    public function __invoke(PutRequest $p_oRequest)
    {
        
        $arrData = $p_oRequest->validated();
        $arrUpdate = [];
        
        if ( !empty( $arrData['type'] ) ) {
            $oType = \App\Models\ExpenseType::where('name', $arrData['type'] )->get();
            $arrUpdate['expense_type'] = $oType->id;
        }
        
        if ( !empty( $arrData['desc'] ) ) {
            $arrUpdate['description'] = $arrData['desc'];
        }

        if ( !empty( $arrData['amount'] ) ) {
            $arrUpdate['amount'] = $arrData['amount'];
        }

        try {

            $isUpdated = \App\Models\Expense::find( $arrData['id'] )->update($arrUpdate);

        } catch (\Exception $e) {            

            return response()->json( 
                [
                    'result' => 0,
                    'reason' => $e->getMessage(),
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );
            
        }

        if ($isUpdated) {

            $arrExpense = \App\Models\Expense::with('type')->find( $arrData['id'] )->toArray();
            $arrExpense['expense_type'] = $arrExpense['type']['name'];
            unset( $arrExpense['type'] );

            return response()->json( 
                [
                    'result' => 1,
                    'data' => $arrExpense
                ],
                JsonResponse::HTTP_OK
            );

        }

        return response()->json( 
            [
                'result' => 0,
                'reason' => 'Expense update failed'
            ],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        );

    }

}