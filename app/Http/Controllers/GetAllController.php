<?php
namespace App\Http\Controllers;

/**
 * HTTP Request Support
 */
use Illuminate\Http\Request;

/**
 * Guzzle HTTP Support
 */
use Illuminate\Support\Facades\Http;

/**
 * JSON Responses
 */
use Illuminate\Http\JsonResponse;

/**
 * Controller to serve API requests for all expenses
 * 
 * @url /expense/all
 * @method GET
 * 
 * @category controllers
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class GetAllController extends Controller
{

    /**
     * Main method to request and serve the data
     * 
     * @url /all
     * @method GET
     * 
     * @return void 
     */
    public function __invoke()
    {

        try {

            $arrData = \App\Models\Expense::all()->toArray();

        } catch (\Exception $e) {

            return response()->json( 
                [
                    'result' => 0,
                    'reason' => $e->getMessage(),
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );

        }

        if ( empty($arrData) ) {

            return response()->json( 
                [
                    'result' => 0,
                    'reason' => 'No expenses data exists',
                ],
                JsonResponse::HTTP_OK
            );

        }

        return response()->json(
            [
                'result' => 1,
                'data' => $arrData
            ],
            JsonResponse::HTTP_OK
        );

    }

}