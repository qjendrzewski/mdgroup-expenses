<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Db gateway model for table `expense_types`
 * 
 * @category models
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class ExpenseType extends Model
{
    use HasFactory;

    protected $table = 'expense_types';

    /**
     * Returns the associated expense
     * 
     * @return Model 
     */
    public function expense()
    {
        return $this->belongsTo(Expense::class, 'expense_type');
    }

}