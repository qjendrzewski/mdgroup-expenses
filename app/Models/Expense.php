<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Db gateway model for table `expenses`
 * 
 * @category models
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class Expense extends Model
{
    use HasFactory;

    protected $table = 'expenses';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * Returns the associated expense-type
     * 
     * @return Model 
     */
    public function type()
    {
        return $this->hasOne(ExpenseType::class, 'id', 'expense_type');
    }

}