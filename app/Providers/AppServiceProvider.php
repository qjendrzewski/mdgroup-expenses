<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        /**
         * Resolving the MySQL migrations error - Syntax error or access violation: 1071 Specified key was too long;
         * @see https://stackoverflow.com/a/42245921/3125602
         */
        Schema::defaultStringLength(191);

    }

}