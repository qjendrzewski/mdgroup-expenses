<?php
namespace App\Logging\Api;

use Monolog\Logger as MonologLogger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\LineFormatter;
use Request;

/**
 * Custom factory to define the Monolog instance at logging
 * 
 * @see https://laravel-tricks.com/tricks/monolog-for-custom-logging
 * @see https://laravel.com/docs/7.x/logging#creating-channels-via-factories
 * 
 * @category custom
 *
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class Logger
{

    public function __invoke(array $arrConfig)
    {

        $oHandler = new RotatingFileHandler( $this->_setLogFile(), 14, MonologLogger::INFO );
        $oHandler->setFormatter(
            new LineFormatter(
                $this->_getLineFormat(),
                null,
                true
            )
        );
        return new MonologLogger( 
            'apilog',
            [
                $oHandler,
            ],
            [
                new WebProcessor
            ]
        );

    }
    
    /**
     * Sets the log file based on the called API
     * 
     * @return string
     */
    protected function _setLogFile()
    {
        $url = request()->path();
        $arrUrl = explode('/', $url);
        $stem = implode('_', $arrUrl);
        $path = storage_path() . "/logs/{$stem}.log";
        return $path;
    }    
    
    /**
     * Returns the line format based on the supplied map array key
     * 
     * @param string $p_key Key
     * 
     * @return string Line format
     */
    private function _getLineFormat(string $p_key=null)
    {
        return "[%datetime%] %channel%.%level_name%: %message%\n";
    }
    
}