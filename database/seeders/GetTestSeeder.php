<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


/**
 * Seeder for table `expenses`
 * Used in /tests/Feature/Http/GetTest.php
 * 
 * @category migrations
 * @subcategory seeders
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class GetTestSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('expenses')->insert(
            [
                [
                    'description' => 'I am one',
                    'amount' => 1.11,
                    'expense_type' => 1,
                ],
                [
                    'description' => 'I am two',
                    'amount' => 2.22,
                    'expense_type' => 2,
                ],
                [
                    'description' => 'I am three',
                    'amount' => 3.33,
                    'expense_type' => 3,
                ],
                [
                    'description' => 'I am four',
                    'amount' => 4.44,
                    'expense_type' => 4,
                ],
                [
                    'description' => 'I am five',
                    'amount' => 5.55,
                    'expense_type' => 5,
                ],
            ]
        );
        
    }

}