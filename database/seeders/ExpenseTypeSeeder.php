<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Seeder for table `expense_types`
 * 
 * @category migrations
 * @subcategory seeders
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class ExpenseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('expense_types')->insert(
            [
                [
                    'name' => 'Entertainment',
                ],
                [
                    'name' => 'Food',
                ],
                [
                    'name' => 'Bills',
                ],
                [
                    'name' => 'Transport',
                ],
                [
                    'name' => 'Other',
                ],
            ]
        );

    }
}
