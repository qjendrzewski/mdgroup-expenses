<?php
namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Expense;

/**
 * Faker factory for Expense model
 * 
 * @category models
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class ExpenseFactory extends Factory
{

    protected $model = Expense::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(5),
            'amount' => $this->faker->randomFloat(2, 50, 100),
            'expense_type' => $this->faker->numberBetween(1, 5),
        ];
    }

}