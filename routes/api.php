<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::prefix('v1/expense')->group(

    function() {

        /**
         * Get all expenses data
         * 
         * @url /api/v1/expense/all
         * @method GET
         */
        Route::get( '/all', [ 'uses' => 'App\Http\Controllers\GetAllController' ] );

        /**
         * Add an expense
         * 
         * @url /api/v1/expense
         * @method POST
         */
        Route::post( '', [ 'uses' => 'App\Http\Controllers\PostController' ] );

        /**
         * Get an expense data
         * 
         * @url /api/v1/expense
         * @method GET
         */
        Route::get( '', [ 'uses' => 'App\Http\Controllers\GetController' ] );

        /**
         * Update an expense
         * 
         * @url /api/v1/expense
         * @method PUT
         */
        Route::put( '', [ 'uses' => 'App\Http\Controllers\PutController' ] );

        /**
         * Delete an expense
         * 
         * @url /api/v1/expense
         * @method DELETE
         */
        Route::delete( '', [ 'uses' => 'App\Http\Controllers\DeleteController' ] );
        
    }

);